package com.helloworld.helloworld.service

import com.helloworld.helloworld.data.AddBookRequest
import com.helloworld.helloworld.data.BookDetailResponse
import com.helloworld.helloworld.model.Author
import com.helloworld.helloworld.model.Book
import com.helloworld.helloworld.repository.BibliotecaRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Service
class BibliotecaService(private val bibliotecaRepository: BibliotecaRepository,
                        private val cassandraClient: CassandraClient) {
    val message = "Welcome to Biblioteca"

    fun getWelcomeMessage(): Mono<String> {
        return Mono.just(message)
    }

    fun create(addBookReq: AddBookRequest): Mono<Book?> {
        val book = Book(UUID.randomUUID(), addBookReq.name, addBookReq.author, addBookReq.publishedYear, addBookReq.isbn)
        return bibliotecaRepository.save(book)
    }

    fun getBooks(): Flux<Book> {
        return bibliotecaRepository.findAll()
    }

    fun getBookById(id: UUID): Mono<Book?> {
        return bibliotecaRepository.findById(id)
    }

    fun getAuthor(): Flux<Author> {
        return cassandraClient.getAuthor()
    }

    fun getAuthorById(id: UUID): Mono<Author> {
        return cassandraClient.getAuthorInfoById(id)
    }

    fun getAuthorsByBookId(id: UUID): Flux<UUID> {
        return cassandraClient.getAuthorsByBookId(id)

    }

    fun getBookDetails(): Flux<BookDetailResponse> {
        return this.getBooks().map { book ->
            val authors = cassandraClient.getAuthorsByBookId(book.id)
                    .flatMap { authorId ->
                        cassandraClient.getAuthorInfoById(authorId)
                    }.collectList().block()!!
            BookDetailResponse(book.name, authors.size, book.yearOfPublish, book.isbn, authors)
        }
    }
}