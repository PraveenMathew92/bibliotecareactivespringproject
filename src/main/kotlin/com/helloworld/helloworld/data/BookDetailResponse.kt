package com.helloworld.helloworld.data

import com.helloworld.helloworld.model.Author
import java.util.*

data class BookDetailResponse(
                              val name: String,
                              val NoOfAuthor: Int,
                              val yearOfPublish : Int,
                              val isbn : String,
                              val authors: List<Author>)