package com.helloworld.helloworld.model

import com.datastax.driver.core.Row
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*

data class Author(
        @JsonIgnore val author_id: UUID,
        val authorname: String,
        val countryofbirth: String,
        val dob: Date) {
constructor(row: Row) : this(
        row.getUUID("author_id"),
        row.getString("authorname"),
        row.getString("countryofbirth"),
        row.getTimestamp("dob")
)
}
